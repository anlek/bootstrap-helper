require "spec_helper"

describe AnlekBootstrapHelper::Renderers::CardRenderer do
  let(:template) { TestViewTemplate.new }
  let(:title) { "MyCard" }
  let(:options) { {} }
  let(:card_c1ontent) { "<b>...content...</b>".html_safe }
  let(:card_outer_content) { "<p>Outer content</p>".html_safe }
  let(:card_footer) { "<p>footer</p>".html_safe }
  let(:card_rendered) do
    AnlekBootstrapHelper::Renderers::CardRenderer.new(template, options) do |p|
      p.header do
        title
      end
      p.content do
        card_c1ontent
      end
      p.outer_content do
        card_outer_content
      end
      p.footer do
        card_footer
      end
    end
  end
  subject(:result) do
    card_rendered.to_html
  end
  let(:expected_output) do
    output = <<-EOS
      <div class="card card-default">
        <div class="header">
          <h3 class="title">#{title}</h3>
        </div>
        <div class="content">
          #{card_c1ontent}
        </div>
        <div>#{card_outer_content}</div>
        <div class="footer">
          #{card_footer}
        </div>
      </div>
    EOS
    output.gsub(/^\s+/, "").gsub("\n", "")
  end

  it "works" do
    expect(subject).to eq expected_output
  end

  context "without blocks" do
    let(:card_rendered) do
      AnlekBootstrapHelper::Renderers::CardRenderer.new(template, options) do |p|
        p.header title
        p.content card_c1ontent
        p.outer_content card_outer_content
        p.footer card_footer
      end
    end

    it "works" do
      expect(subject).to eq expected_output
    end
  end

  context 'options' do
    context 'id' do
      let(:id) { "my_id" }
      let(:options) { {id: id} }
      it "can be set" do
        expect(subject).to have_css("##{id}.card")
      end
    end
    context "type" do
      let(:type) { "warning" }
      let(:options) { {type:type} }
      it "can be set" do
        expect(subject).to have_css(".card.card-#{type}")
      end
    end
  end

  context 'header' do
    it "defaults to title (h3)" do
      expect(subject).to have_css ".header > h3.title"
    end

    context 'change tag for title' do
      let(:card_rendered) do
        AnlekBootstrapHelper::Renderers::CardRenderer.new(template, options) do |p|
          p.header tag: :h5 do
            title
          end
        end
      end
      it 'works' do
        expect(subject).to have_css ".header > h5.title"
      end
    end
    context 'not a title' do
      let(:card_rendered) do
        AnlekBootstrapHelper::Renderers::CardRenderer.new(template, options) do |p|
          p.header title: false do
            title
          end
        end
      end
      it "works" do
        expect(subject).to have_css ".header"
        expect(subject).to_not have_css ".header > .title"
      end
    end
  end
end
