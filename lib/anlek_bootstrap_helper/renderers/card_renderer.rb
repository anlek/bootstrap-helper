module AnlekBootstrapHelper
  module Renderers
    class CardRenderer < Base
      BASE_CSS_CLASS = "card"

      attr_reader :context
      attr_accessor :id, :title
      def initialize(template, options={}, &block)
        super template, options
        parse_options
        @context = CardContext.new(self)
        block.call(@context) if block_given?
      end

      def to_html
        content_tag(:div, options) do
          raw context.parts.join
        end
      end
      alias to_s to_html

      #######
      private
      #######

      def parse_options
        options[:class] = append_class(options[:class], BASE_CSS_CLASS, "#{BASE_CSS_CLASS}-#{ options.delete(:type) || "default" }")
      end

    end

    class CardContext < BaseContext
      attr_reader :parts
      def initialize (renderer)
        super renderer
        @parts = []
      end

      def header content_or_as_title=nil, title: true, tag: :h3, &block
        title = content_or_as_title if boolean?(content_or_as_title)
        @parts << content_tag(:div, class: "header") do
          content = extract_content(content_or_as_title, &block)
          options = title ? {class: "title"} : {}
          content_tag(tag, content, options)
        end
      end
      def content content=nil, &block
        content = content_tag(:div, extract_content(content, &block), class: "content")
        @parts << content
      end
      def outer_content content="", &block
        @parts << content_tag(:div, extract_content(content, &block))
      end

      def footer(content="", &block)
        @parts << content_tag(:div, extract_content(content, &block), class: "footer")
      end

      #######
      private
      #######

      def boolean?(object)
        object.is_a?(TrueClass) || object.is_a?(FalseClass)
      end

      def extract_content(content=nil, &block)
        if block_given?
          capture(&block)
        else
          content
        end
      end

    end
  end
end