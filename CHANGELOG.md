# Changelog
All notable changes to this project will be documented in this file.

## 1.0.2 - 2017-05-01

### Added
- Ability to specify the type of badge to render
- Ability to specify 'pill' on a badge

## 1.0.1 - 2016-03-05

### Fixed
- Fixed bug in cancan permissions delete/destroy

## 1.0 - 2016-02-10
### Added
- Added ability Card based render (using {Light Dashboard Pro}[http://www.creative-tim.com/product/light-bootstrap-dashboard-pro] card format)

### Fixed
- Fixed a few issues in the README, making it cleaner and fixing deprecated `style` usage in panel

## 0.1 - 2015-11-05
### Added
- Added ability to use `view_btn_for Model` to render a show button.

### Fixed
- Fixed issue with model names that are multiworded. IE: MembershipFee used to be "Add MembershipFee",
  now it will show up as "Add Membership fee".

## 0.0.17.3 - 2015-01-14
### Fixed
- Fixed issue with how header block was being rendered in list_group
- Fixed wrong context in testing

## 0.0.17.2 - 2015-01-14
### Fixed
- page_header:
  - page_header now takes a block.
  - Changed how subtitle is passed to the page_header.
  - page_header now can have the h tag specified

## 0.0.17.1 - 2015-01-08
### Added
- Added ability to render panels without blocks

## 0.0.17 - 2015-01-07
### Added
- Added tb_label
- Added tb_badge

## 0.0.16.4 - 2015-01-03
### Added
- Added ability to specify spacing for list group item `to_html` method

### Fixed
- Fixed list group item text and heading rendering with a block
- Refactored code

## 0.0.16.3 - 2015-01-03
### Fixed
- Fixed issue rendering list group item text with a block


## 0.0.16.2 - 2014-12-29
### Fixed
- Removed join issue on list group


## 0.0.16.1 - 2014-12-29
### Fixed
- Fixed rendering issue with list groups



## 0.0.16 - 2014-12-29
### Added
- Added ability to render list groups



## 0.0.15 - 2014-10-14
### Added
- Added ability to send array values to crud_btns


## 0.0.14 - 2014-10-04
### Added
- cancel_btn_for

## 0.0.13 - 2014-07-25
### Fixed
- Fixed the order the panel-body tag and the panel-collapse tag are rendered.
- Fixed collapse_id not being set in panel-collapse.

## 0.0.12 - 2014-07-21
### Added
- Added ability to make collapsable panel

### Deprecated
- the use of :style is no longer recommended in panel. Please use :type

## 0.0.11 - 2014-07-21
### Removed
- automatic naming of edit/delete buttons with the model name

## 0.0.10 - 2014-06-24
### Added
- Crud buttons take :label instead of :name for button label
- Added **CHANGELOG**

### Deprecated
- The use of :name is no longer recommended in crud buttons.

## 0.0.2 - UNKNOWN
### Added
- Inital Release
- ALL CHANGES HAPPENED BEFORE CHANGELOG WAS CREATED
