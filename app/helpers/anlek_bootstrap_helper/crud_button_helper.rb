module AnlekBootstrapHelper
  module CrudButtonHelper
    extend ActiveSupport::Concern
    include ButtonHelper
    STYLE = {
      list: { icon: 'arrow-left', type: 'default' },
      show: { icon: 'eye', type: 'primary' },
      new: { icon: 'plus', type: 'primary' },
      edit: { icon: 'pencil', type: 'success' },
      delete: { icon: 'times', type: 'danger' }
    }.freeze

    def list_btn_for(models, options = {})
      models  = Array.wrap(models)
      options = stylize_options(options, :list)
      model   = models.last
      label   = extract_label(
        options,
        "Back to #{pretty_name(model.name.to_s.pluralize)}"
      )
      link_to_btn(label, polymorphic_path(models), options) if cancan? :read, models
    end

    def view_btn_for(models, options = {})
      models  = Array.wrap(models)
      options = stylize_options(options, :show)
      label   = extract_label options, 'View'
      link_to_btn(label, polymorphic_path(models), options) if cancan? :show, models
    end

    def new_btn_for(models, options = {})
      models  = Array.wrap(models)
      options = stylize_options(options, :new)
      model   = models.last
      label   = extract_label options, "Add #{pretty_name(model.name)}"
      link_to_btn(label, polymorphic_path(models, action: :new), options) if cancan? :create, models
    end

    def edit_btn_for(models, options = {})
      models  = Array.wrap(models)
      options = stylize_options(options, :edit)
      label   = extract_label options, 'Edit'
      link_to_btn(label, polymorphic_path(models, action: :edit), options) if cancan? :edit, models
    end

    def delete_btn_for(models, options = {})
      models  = Array.wrap(models)
      options = append_delete_options(stylize_options(options, :delete))
      label   = extract_label options, 'Remove'
      link_to_btn(label, models, options) if cancan? :destroy, models
    end

    #######
    private
    #######

    def cancan?(type, models)
      return true unless AnlekBootstrapHelper.using_cancan?
      can? type, models.last
    end

    def pretty_name(name)
      name.to_s.underscore.humanize
    end

    def append_delete_options(options = {})
      data             = options.delete(:data) || {}
      data[:method]  ||= options.delete(:method) || 'DELETE'
      data[:confirm] ||= options.delete(:confirm) || 'Are you sure?'
      options[:data]   = data
      options
    end

    def stylize_options(options, style)
      options.symbolize_keys!
      options.reverse_merge(STYLE[style])
    end

    def extract_label(options, default = '')
      if options.key?(:name)
        ActiveSupport::Deprecation.warn(
          'option(:name) is no longer used, please use option(:label)'
        )
        options[:label] ||= options.delete(:name)
      end
      options.delete(:label) || default
    end
  end
end
