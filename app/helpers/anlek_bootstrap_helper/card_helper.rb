module AnlekBootstrapHelper
  # Used to generate bootstrap cards
  module CardHelper
    extend ActiveSupport::Concern
    def tb_card (options={}, &block)
      AnlekBootstrapHelper::Renderers::CardRenderer.new(self, options, &block).to_html
    end
  end
end