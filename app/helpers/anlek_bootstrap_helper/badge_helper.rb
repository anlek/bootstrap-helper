module AnlekBootstrapHelper
  module BadgeHelper
    extend ActiveSupport::Concern
    include BaseHelper
    include IconHelper

    def tb_badge(text, options = {})
      options.stringify_keys!

      tag = options.delete('tag') || :span

      type = options.delete('type') || 'default'

      apply_class = ['badge', "badge-#{type}"]

      pill_style = options.delete('pill').present?

      apply_class << 'badge-pill' if pill_style

      options['class'] = append_class(
        options['class'],
        apply_class
      )

      icon = icon_for(options.delete('icon'))

      content = safe_join([icon, ' ', text])

      content_tag(tag, content, options)
    end
  end
end
